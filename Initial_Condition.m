function x0_1 = Initial_Condition(N,zd,z,x0_z)
    Phi = sym('phi',[N-1,N])*0;
    i = 1;
    Phi(i,i) = (-z+zd(i+1))/(zd(i+1)-zd(i));
    for i = 2:N-1
        Phi(i-1,i) = (z-zd(i-1))/(zd(i)-zd(i-1));
        Phi(i,i) = (-z+zd(i+1))/(zd(i+1)-zd(i));
    end
    i = N;
    Phi(i-1,i) = (z-zd(i-1))/(zd(i)-zd(i-1));
    M = zeros(N,1);
    i = 1;
    M(i) = eval(int(Phi(i,i)*x0_z,z,zd(i),zd(i+1)));
    for  i = 2:N-1
        M(i) = eval(int(Phi(i-1,i)*x0_z,z,zd(i-1),zd(i))) + eval(int(Phi(i,i)*x0_z,z,zd(i),zd(i+1)));
    end
    i = N;
    M(i) = eval(int(Phi(i-1,i)*x0_z,z,zd(i-1),zd(i)));
    E = E_matrix(zd,N);

    x0_1 = E\M;
end