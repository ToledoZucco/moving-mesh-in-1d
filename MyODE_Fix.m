function Edxdt = MyODE_Fix(t,x)
%     u = 1-heaviside(t-1);
%     u = sin(t);
    u = 0 + 0*heaviside(t-0.5);
    N = length(x)/2;
    z = x(N+1:2*N);
    x = x(1:N);
    
    
    
    h = 1/(N-1);
    z_mean = 0:h:1;
    dz = zeros(N,1);
    for i = 1:N
        if z(i) <= z_mean(i)
            z(i) = z_mean(i);
            dz(i) = 0;
        else
            dz(i) = -1*0;
        end
        z(i) = z_mean(i);
    end
    z(1) = 0; dz(1) = 0;
    z(N) = 1; dz(N) = 0;
    
    i = 1;
    ii = [i i i+1 i+1];
    jj = [i i+1 i i+1];
    ss = [1/3 -1/3 1/6 -1/6];
    M{1} = sparse(ii, jj, ss,N,N);
    for i = 2:N-1

        ii = [i-1 i-1 i i i i+1 i+1];
        jj = [i-1 i i-1 i i+1 i i+1];
        ss = [1/6 -1/6 1/3 0 -1/3 1/6 -1/6];

        M{i} = sparse(ii,jj,ss,N,N);
    end
    i = N;
    ii = [i-1 i-1   i   i ];
    jj = [i-1 i     i-1 i ];
    ss = [1/6 -1/6  1/3 -1/3];
    M{i} = sparse(ii,jj,ss,N,N);
    %At = M{1}*dz1dt + M{2}*dz2dt +....
    
    At = zeros(N,N);
    for i = 1:N
        At = At + M{i}*dz(i);
    end
    
    ii = [1     N       1:N-1               1+1:N];
    jj = [1     N       1+1:N               1:N-1];
    ss = [-1/2  1/2     -1/2*ones(1,N-1)    1/2*ones(1,N-1)];
    Az = sparse(ii,jj,ss,N,N);

    Phi_a = sparse(1,1,1,N,1);
    
    F = -Phi_a*Phi_a' - Az - At;

    B = sparse(N,1,1,N,1);
    
    Edxdt = F*x + B*u;
    Edxdt = [Edxdt;dz];
end