
function [E,J,R,Q,B,C,D,h] = Transport_Equation(a,b,H,N)


h = (b-a)/(N-1);
%% E matrix
id = (1:N)';
jd = (1:N)';
md = [h/3;2*h/3*ones(N-2,1);h/3];

isd = (1:N-1)';
jsd = (2:N)';
msd = h/6*ones(N-1,1);

iid = (2:N)';
jid = (1:N-1)';
mid = h/6*ones(N-1,1);

im = [id;isd;iid];
jm = [jd;jsd;jid];
mm = [md;msd;mid];

E = sparse(im,jm,mm,N,N);

% full(E)

Q = H*E;


%% D matrix
id = (1:N)';
jd = (1:N)';
dd = [-1/2;zeros(N-2,1);1/2];

isd = (1:N-1)';
jsd = (2:N)';
dsd = -1/2*ones(N-1,1);

iid = (2:N)';
jid = (1:N-1)';
did = 1/2*ones(N-1,1);

im = [id;isd;iid];
jm = [jd;jsd;jid];
dm = [dd;dsd;did];


D1 = sparse(im,jm,dm,N,N);

% full(D1)

i11     = 1;
j11     = 1;
phi11   = 1;
Phi_a = sparse(i11,j11,phi11,N,1);

D2 = Phi_a*Phi_a';

% full(D2)

A = -D1-D2;
J = 1/2*(A-A');
R = -1/2*(A+A');

% full(J)
% full(R)
% full(A-(J-R))
%% B matrix
iN1 = N;
jN1 = 1;
bN1 = 1;

B = sparse(iN1,jN1,bN1,N,1);

% full(B)


%% C matrix
i11 = 1;
j11 = 1;
c11 = 1;

C = sparse(i11,j11,c11,1,N)*H;

% full(C)
D = sparse(1,1,0,1,1);

end