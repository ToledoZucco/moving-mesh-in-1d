%Developper     : Jesus-Pablo Toledo-Zucco
%Date           : 02/02/2024

%This codes proposed a Simulation of the one-dimensional transport equation
%using the finite element method such that the scattering-energy preserving
%structure is preserved. It is compared the usual case of a fixed mesh with
%the case of a moving mesh.

%This code has to be optimized and the use of the symbolic toolbox should
%be avoided

close all
clear all
clc

%% Plot parameters
lw = 3;
ms = 20;
fs  = 30;
fst = fs*0.8;
x0screen=100;y0screen=100;WidthScreen=1200;HeightScreen=400;
Blue = [0 0.4470 0.7410];
Orange = [0.8500 0.3250 0.0980];
Yellow = [0.9290 0.6940 0.1250];
Violet = [0.4940 0.1840 0.5560];
Green = [0.4660 0.6740 0.1880];
Cyan = [0.3010 0.7450 0.9330];
quality = '-r210';



%% Moving mesh
N = 20;

%Initial condition
f = 10;
syms z positive
x0_z = 2*exp(-420*(z-0.8).^2);
% x0_z = sin(2*pi*f*z);

%Fixed case
z0_1 = linspace(0,1,N)';
% z0_1 = nonLinspace(0,1,N,'log10')';
x0_1 = Initial_Condition(N,z0_1,z,x0_z);
% z0 = linspace(0,1,N)';
opts = odeset('Mass',@mass_Fix,'MStateDependence','strong');
tspan = 0:0.01:2;
[t,x] = ode15s(@MyODE_Fix,tspan,[x0_1;z0_1],opts);
t1 = t';
x1 = x';
z1  = x1(N+1:2*N,:);
x1 = x1(1:N,:);
y1 = x1(1,:);

%Moving case
z0_2 = nonLinspace(0,1,N,'log10')';
% z0_2 = z0_1;
x0_2 = Initial_Condition(N,z0_2,z,x0_z);
opts = odeset('Mass',@mass_Moving,'MStateDependence','strong');
[t,x] = ode15s(@MyODE_Moving,tspan,[x0_2;z0_2],opts);
t2 = t';
x2 = x';
z2  = x2(N+1:2*N,:);
x2 = x2(1:N,:);
y2 = x2(1,:);

%%
xoff =-0.06; yoff = 0.07; Widthoff = 0.13; Heightoff = -0.030;

figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])

hold on
for i = 1:N
p = plot(t1,z1(i,:),'--','LineWidth',lw/2);
ColorPlot{i} = p.Color;
end

for i = 1:N
plot(t2,z2(i,:),'Color',ColorPlot{i},'LineWidth',lw/2)
end
set(gca,'Fontsize',fst)
Pos = get(gca,'Position');
set(gca,'Position',Pos+[xoff yoff Widthoff Heightoff])
xlabel('time $t$ $(s)$','Interpreter','latex','FontSize',fs)
ylabel('Trajectories of the nodes','Interpreter','latex','FontSize',fs)
grid on
% print(gcf,'-dpng',quality,'Trajectories.png');


zeta = linspace(z1(1,1),z1(N,1),1000);
Phi_1 = Projection(z1(:,1),zeta,N);
Phi_2 = Projection(z2(:,1),zeta,N);
%%
% HeightScreen = 350;
figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])
hold on
for i = 1:N
    p = plot(zeta,Phi_1(i,:),'LineWidth',lw);
%     plot(zeta,Phi_2(i,:),'Color',p.Color,'LineWidth',lw)
    leg_phi{i} = sprintf('$\\phi_{%d} (\\zeta)$',i);
end
set(gca,'FontSize',fst)
xlabel('space $\zeta$ $(m)$','Interpreter','latex','FontSize',fs)
ylabel('Basis functions $\Phi(\zeta)$','Interpreter','latex','FontSize',fs)
leg_plot = legend(leg_phi,'Interpreter','latex','FontSize',fs*0.9,'Location','northoutside');
leg_plot.NumColumns = N/2;
xoff =-0.05; yoff = -0.01; Widthoff = 0.13; Heightoff = 0.05;
    
Pos = get(gca,'Position');
set(gca,'Position',Pos+[xoff yoff Widthoff Heightoff])
% print(gcf,'-dpng',quality,'InitialMesh_Fix.png');





figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])
hold on
for i = 1:N
%     p = plot(zeta,Phi_1(i,:),'--','LineWidth',lw);
    plot(zeta,Phi_2(i,:),'LineWidth',lw)
    leg_phi{i} = sprintf('$\\phi_1^{%d} (\\zeta)$',i);
end
set(gca,'FontSize',fst)
xlabel('space $\zeta$ $(m)$','Interpreter','latex','FontSize',fs)
ylabel('Basis functions $\Phi(\zeta,0)$','Interpreter','latex','FontSize',fs)
leg_plot = legend(leg_phi,'Interpreter','latex','FontSize',fs*0.9,'Location','northoutside');
leg_plot.NumColumns = N/2;
Pos = get(gca,'Position');
set(gca,'Position',Pos+[xoff yoff Widthoff Heightoff])


% print(gcf,'-dpng',quality,'InitialMesh_Moving.png');
%%

xap1 = 0*zeta;
xap2 = 0*zeta;
for i = 1:N
    xap1 = xap1 + Phi_1(i,:)*x1(i,1);
    xap2 = xap2 + Phi_2(i,:)*x2(i,1);
end

h_exact = mean(diff(zeta));


for ii = 1:1:length(t)
    
    Phi_1 = Projection(z1(:,ii),zeta,N);
    Phi_2 = Projection(z2(:,ii),zeta,N);
    xap1 = 0*zeta;
    xap2 = 0*zeta;
    for i = 1:N
        xap1 = xap1 + Phi_1(i,:)*x1(i,ii);
        xap2 = xap2 + Phi_2(i,:)*x2(i,ii);
    end
    z = zeta + t(ii);
    ExactSol = eval(x0_z);
    x_exact(:,ii) = ExactSol;
    yexact(ii) = ExactSol(1);
    
    E1 = mass_Fix(t(ii),[x1(:,ii);z1(:,ii)]);
    E2 = mass_Moving(t(ii),[x2(:,ii);z2(:,ii)]);
  
    E1 = E1(1:N,1:N);
    E2 = E2(1:N,1:N);
    
    H11(ii) = 1/2*x1(:,ii)'*E1*x1(:,ii);
    H2(ii) = 1/2*x2(:,ii)'*E2*x2(:,ii);
    H1(ii) = 1/2*sum(xap1.^2)*h_exact; 
    H2(ii) = 1/2*sum(xap2.^2)*h_exact; 
    Hexact(ii) = 1/2*sum(x_exact(:,ii).^2)*h_exact; 
end
%%
HeightScreen = 400;

figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])
hold on
plot(t1,y1,'LineWidth',lw)
plot(t2,y2,'LineWidth',lw)
plot(t,yexact,'LineWidth',lw,'Color',Green)
set(gca,'Fontsize',fst)
xoff =-0.050; yoff = 0.07; Widthoff = 0.12; Heightoff = -0.05;
Pos = get(gca,'Position');
set(gca,'Position',Pos+[xoff yoff Widthoff Heightoff])


% title('Output','Interpreter','latex','FontSize',fs)
legend({'Static mesh','Moving Mesh','Exact Solution'},'Interpreter','latex','FontSize',fs,'Location','northwest')
xlabel('time $t$ $(s)$','Interpreter','latex','FontSize',fs)
ylabel('Output $y(t) = x(a,t)$','Interpreter','latex','FontSize',fs)
grid on
% print(gcf,'-dpng',quality,'Output.png');




figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])
hold on
plot(t1,H1,'LineWidth',lw)
% plot(t1,H11,'--','LineWidth',lw)
plot(t2,H2,'LineWidth',lw)
plot(t,Hexact,'LineWidth',lw,'Color',Green)
set(gca,'Fontsize',fst)
Pos = get(gca,'Position');
set(gca,'Position',Pos+[xoff yoff Widthoff Heightoff])


% title('Hamiltonian','Interpreter','latex','FontSize',fs)
legend({'Static mesh','Moving Mesh','Exact Solution'},'Interpreter','latex','FontSize',fs,'Location','northeast')
xlabel('time $t$ $(s)$','Interpreter','latex','FontSize',fs)
ylabel('Hamiltonian $H(t)$','Interpreter','latex','FontSize',fs)
grid on
% print(gcf,'-dpng',quality,'Energy.png');



%%
HeightScreen = 400;
% figureDirectory = 'Animation';
% filename = 'Fig'; 
% 
% if isfolder(figureDirectory) == 1 % If the file figureDirectory exists, delete it with all the files insisde
%     rmdir(figureDirectory,'s');
%     pause(0.1);
% end
% 
% mkdir(figureDirectory); %Othercase, create the file figureDirectory
% pause(0.1);

z = zeta - t(1);
ExactSol = eval(x0_z);
figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])
hold on
p1 = plot(zeta,xap1,'LineWidth',lw);
p2 = plot(zeta,xap2,'LineWidth',lw);
p_exact = plot(zeta,ExactSol,'LineWidth',lw,'Color',Green);
set(gca,'Fontsize',fst)
xoff =-0.050; yoff = 0.066; Widthoff = 0.13; Heightoff = -0.015;
Pos = get(gca,'Position');
set(gca,'Position',Pos+[xoff yoff Widthoff Heightoff])


% title('Static mesh versus moving mesh','Interpreter','latex','FontSize',fs)
legend({'Static mesh','Moving Mesh','Exact Solution'},'Interpreter','latex','FontSize',fs,'Location','northwest')
xlabel('space $\zeta$ $(m)$','Interpreter','latex','FontSize',fs)
ylabel(sprintf('Num Sol at $t =%.1f $',t(1)),'Interpreter','latex','FontSize',fs)
grid on
xlim([0,1])
ylim([-1,2.5])
j = 1;

t_selected = [1,200,400,600];
t_selected = 1:length(t);

for jj = 1:1:length(t_selected)
    ii = t_selected(jj);
    
    Phi_1 = Projection(z1(:,ii),zeta,N);
    Phi_2 = Projection(z2(:,ii),zeta,N);
    xap1 = 0*zeta;
    xap2 = 0*zeta;
    for i = 1:N
        xap1 = xap1 + Phi_1(i,:)*x1(i,ii);
        xap2 = xap2 + Phi_2(i,:)*x2(i,ii);
    end
    set(p1,'YData',xap1)
    set(p2,'YData',xap2)

    set(p_exact,'YData',x_exact(:,ii))
    ylabel(sprintf('Num Sol at $t =%.2f$ $(s)$',t(ii)),'Interpreter','latex','FontSize',fs)
    
    if jj == 4
        legend({'Static mesh','Moving Mesh','Exact Solution'},'Interpreter','latex','FontSize',fs,'Location','northeast')
    end
%     print(gcf,'-dpng',quality,[figureDirectory,'/',filename,num2str(j),'.png']);
    j = j+1;
    pause(0.005)

end

