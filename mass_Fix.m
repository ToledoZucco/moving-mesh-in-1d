function E = mass_Fix(t,y)
   
    N = length(y)/2;
    z = y(N+1:2*N);
    
    
    ii = [1 2];
    jj = [1 2];
    ss = [-1 -1]/3;

    ii = [ii,[1 2]];
    jj = [jj,[2 1]];
    ss = [ss,[-1 -1]/6];

    EM{1} = sparse(ii, jj, ss,N,N);
    for i = 2:N-1

        ii = [i-1 i+1];
        jj = [i-1 i+1];
        ss = [1 -1]/3;

        ii = [ii,[i-1 i i i+1]];
        jj = [jj,[i i-1 i+1 i]];
        ss = [ss,[1 1 -1 -1]/6];

        EM{i} = sparse(ii,jj,ss,N,N);
    end
    ii = [N-1 N];
    jj = [N-1 N];
    ss = [1 1]/3;
    ii = [ii,[N-1 N]];
    jj = [jj,[N N-1]];
    ss = [ss,[1 1]/6];
    EM{N} = sparse(ii, jj,ss,N,N);

    E = zeros(N,N);
    for i = 1:N
        E = E + EM{i}*z(i);
    end
%     
%     h = 1/(N-1);
%     z_mean = 0:h:1;
%     for i = 1:N
%         if z(i) <= z_mean(i)
%             z(i) = z_mean(i);
%         end
%     end
    
    
    E = blkdiag(E,eye(N));
end