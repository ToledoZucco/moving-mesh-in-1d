close all
clear all
clc

PlotParameters

figureDirectory = '../Figs/Sim_MainProblem';
filename = 'Sim'; 

%Delay approximation
%High-fidelity model of the transport equation
D       = 4;      %Delay in seconds
a       = 0;      %left boundary of the PDE
b       = D;      %right boundary of the PDE
H       = 1;      %Hamiltonian density

t = 0:0.001:10;
u = heaviside(t-1);
% %% Delay approximation using finite element method
N_d    = 80;    %Number of states for the approximation
figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])
subplot(1,2,1)
hold on
p_d = plot(1,1,'x','MarkerSize',ms,'LineWidth',lw);
p_p = plot(1,1,'x','MarkerSize',ms,'LineWidth',lw);
set(gca,'FontSize',fst)
legend('Eig PFEM', 'Eig PADE','Interpreter','latex','Location','northwest')
grid on
xlim([-60,20])
ylim([-60,60])
xlabel('Real','Interpreter','latex')
ylabel('Imaginary','Interpreter','latex')
title_1 = title(sprintf('$N=2$'),'Interpreter','latex');

subplot(1,2,2)
hold on
p_step_d = plot(1,1,'MarkerSize',ms,'LineWidth',lw*2);
p_step_p = plot(1,1,'MarkerSize',ms,'LineWidth',lw);
plot(t,u,'MarkerSize',ms,'LineWidth',lw);
set(gca,'FontSize',fst)

legend('Step response PFEM', 'Step response PADE','Interpreter','latex','Location','northwest')
grid on
xlabel('time $(s)$','Interpreter','latex')
ylabel('Responses','Interpreter','latex')
title_2 = title(sprintf('$N=2$'),'Interpreter','latex');
xlim([0,t(end)])
ylim([-1,2])
for N_d = 2:2:N_d
[E_dD,J_dD,R_dD,Q_dD,B_dD,C_dD,D_dD,h_dD] = Transport_Equation(a,b,H,N_d);
zeta_dD    = a:h_dD:b;
A_dD       = (J_dD-R_dD)*H;

SYS_d = dss(A_dD,B_dD,C_dD,D_dD,E_dD);


% %% Delay approximation using PADE
s = tf('s');
SYS_p = pade(exp(-s*D)*1,N_d);


Eig_d = eig(SYS_d);
Eig_p = eig(SYS_p);

X0 = zeros(N_d,1);
[Y_d,T_d,~] = lsim(SYS_d,u,t,X0);
[Y_p,T_p,~] = lsim(SYS_p,u,t,X0);

set(p_d,'XData',real(Eig_d),'YData',imag(Eig_d))
set(p_p,'XData',real(Eig_p),'YData',imag(Eig_p))
set(title_1,'String',sprintf('$N=%i$',N_d))

set(p_step_d,'XData',T_d,'YData',Y_d)
set(p_step_p,'XData',T_p,'YData',Y_p)
set(title_2,'String',sprintf('$N=%i$',N_d))

print(gcf,'-dpng','-r80',[figureDirectory,'/',filename,num2str(k),'.png']);

pause(0.1)
end
figure
step(SYS_d,SYS_p)
