close all
clear all
clc

PlotParameters
% MyHeaviside = @(t) 0.5*(t == 0) + (t > 0);


% %% Open-loop System without delay
c = 0.1;        %Damping coefficient
k = 1;          %Spring coefficient
m = 1;          %Mass
%Mass-Spring-Damper system
A = [0,1;-1,-c]*diag([k,1/m]);
B = [0;1];
C = B'*diag([k,1/m]);
C = eye(2);
[p,n] = size(C);

%Delay approximation
%High-fidelity model of the transport equation
D       = 4;      %Delay in seconds
a       = 0;      %left boundary of the PDE
b       = D;      %right boundary of the PDE
H       = 1;      %Hamiltonian density
N_HF    = 200;    %Number of states for the approximation

[E_HFD,J_HFD,R_HFD,Q_HFD,B_HFD,C_HFD,D_HFD,h_HFD] = Transport_Equation(a,b,H,N_HF);
zeta_HFD    = a:h_HFD:b;
A_HFD       = (J_HFD-R_HFD)*H;

E_A_HF = blkdiag(eye(n),E_HFD);
A_A_HF = [A,B*C_HFD;zeros(N_HF,n),A_HFD];
B_A_HF = [zeros(n,1);B_HFD];
C_A_HF = [C,zeros(p,N_HF)];
D_A_HF = 0;

SYS_A_HF = dss(A_A_HF,B_A_HF,C_A_HF,D_A_HF,E_A_HF);

% step(SYS_A_HF)


% %% Delay approximation using finite element method
N_d    = 4;    %Number of states for the approximation
figure
hold on
p_d = plot(1,1,'x','MarkerSize',ms,'LineWidth',lw);
p_p = plot(1,1,'x','MarkerSize',ms,'LineWidth',lw);
legend('Eig PFEM', 'Eig PADE','Interpreter','latex')
grid on
% xlim([-100,2])
% ylim([-60,60])
for N_d = N_d:N_d
[E_dD,J_dD,R_dD,Q_dD,B_dD,C_dD,D_dD,h_dD] = Transport_Equation(a,b,H,N_d);
zeta_dD    = a:h_dD:b;
A_dD       = (J_dD-R_dD)*H;

E_A_d = blkdiag(eye(n),E_dD);
A_A_d = [A,B*C_dD;zeros(N_d,n),A_dD];
B_A_d = [zeros(n,1);B_dD];
C_A_d = [C,zeros(p,N_d)];
D_A_d = 0;

SYS_A_d = dss(A_A_d,B_A_d,C_A_d,D_A_d,E_A_d);

% step(SYS_A_HF,SYS_A_d)

% %% Delay approximation using PADE
s = tf('s');
SYS_A_p = pade(exp(-s*D)*C*(s*eye(n)-A)^(-1)*B,N_d);

% SYS_A_p = pade(exp(-s*D),N_d);
% 
% SYS_A_p = ss(SYS_A_p);
% 
% E_pD = eye(N_d);
% A_pD = SYS_A_p.A;
% B_pD = SYS_A_p.B;
% C_pD = SYS_A_p.C;
% D_pD = SYS_A_p.D;
% 
% E_A_p = blkdiag(eye(n),E_pD);
% A_A_p = [A,B*C_pD;zeros(N_d,n),A_pD];
% B_A_p = [zeros(n,1);B_pD];
% C_A_p = [C,zeros(p,N_d)];
% D_A_p = 0;
% 
% SYS_A_p = dss(A_A_p,B_A_p,C_A_p,D_A_p,E_A_p);
% 



Eig_d = eig(SYS_A_d);
Eig_p = eig(SYS_A_p);

set(p_d,'XData',real(Eig_d),'YData',imag(Eig_d))
set(p_p,'XData',real(Eig_p),'YData',imag(Eig_p))
pause(0.1)
end
figure
step(SYS_A_HF,SYS_A_d,SYS_A_p)


%%


%Small Delay to show desired behaviour
Dsmall = 0.01;

%Time, input and initial conditions
t = linspace(0,20,16000);
% u_ref = MyHeaviside(t-1);
% u_ref = sin(2*pi*t).*(1-MyHeaviside(t-1));
u_ref = (1-exp(-20*(t-1))).*MyHeaviside(t-1);
X0 = [0;0];




%% Open loop System with Delay 


E_A_HF = blkdiag(E_HFD,eye(n));
A_A_HF = [A_HFD,zeros(N_HF,n);B*C_HFD,A];
B_A_HF = [B_HFD;zeros(n,1)];
C_A_HF = [zeros(1,N_HF),C];
D_A_HF = 0;

SYS_a = dss(A_A_HF,B_A_HF,C_A_HF,D_A_HF,E_A_HF);
[y_d,t,Z_a] = lsim(SYS_a,u_ref,t,[z0;X0]);
Z_a = Z_a';
y_d = y_d';

z_a = Z_a(1:N_HF,:);
x_a = Z_a(N_HF+1:N_HF+n,:);

x1_a = x_a(1,:);
x2_a = x_a(2,:);

u = u_ref;


figure
set(gcf,'units','points','position',[x0screen,y0screen+1.25*HeightScreen,WidthScreen,HeightScreen])
hold on
plot(t,u,'LineWidth',lw,'MarkerSize',ms)
% plot(t,y_d,'LineWidth',lw,'MarkerSize',ms)
% plot(t,X,'--','LineWidth',lw,'MarkerSize',ms)
plot(t,x1_a,'LineWidth',lw,'MarkerSize',ms)
plot(t,x2_a,'LineWidth',lw,'MarkerSize',ms)
set(gca,'FontSize',fst)
title(sprintf('Open-loop Simulation with input delay $d = %2.2f$ $s$',D),'interpreter','latex','Fontsize',fs)
xlabel('time $t$ $(s)$','interpreter','latex','Fontsize',fs)
legend({'$u(t)$','$x_1(t)$','$x_2(t)$'},'interpreter','latex','Fontsize',fs)

w = z_a;

xSpace = 0.05;
x0Plot = 0.05;
y0Plot = 0.15;
WidthPlot = 0.2;
HeightPlot = 0.7;

figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])

Pos1 = [x0Plot,y0Plot,WidthPlot,HeightPlot];

ax1 = axes('Position',Pos1);
p1 = plot(t(1),u(1),'.-','Color',Blue,'LineWidth',lw);
set(gca,'FontSize',fst)
leg1{1} = sprintf('$u(t)$');
legend(leg1,'Interpreter','latex','FontSize',fs,'Location','southwest')
title('Input $u(t)$','Interpreter','latex','FontSize',fs)
xlabel('time $(s)$','Interpreter','latex','FontSize',fs)
ymax = 1.1*max(max(abs(u)));
xlim([t(1),t(end)])
ylim([-ymax,ymax])
grid on


Pos2 = Pos1 + [xSpace + Pos1(3),0,WidthPlot,0];
ax2 = axes('Position',Pos2);
p2 = plot(zeta_HFD,w(:,1),'.-','Color',Blue,'LineWidth',lw);
set(gca,'FontSize',fst, 'Xdir', 'reverse')
leg2{1} = sprintf('$z(\\zeta,t)$');
legend(leg2,'Interpreter','latex','FontSize',fs,'Location','southwest')
title(sprintf('Delay or Transport equation $d = %2.2f$ $s$',D),'Interpreter','latex','FontSize',fs)
xlabel('space $(m)$','Interpreter','latex','FontSize',fs)
ymax = 1.1*max(max(abs(w)));
xlim([a,b])
ylim([-ymax,ymax])

ytickformat('%.2f')
xticks([a b])
yticks([-ymax,0,ymax])
grid on


Pos3 = Pos2 + [xSpace + Pos2(3),0,-WidthPlot,0];
ax3 = axes('Position',Pos3);
hold on
p31 = plot(t(1),x1_a(1),'.-','Color',Blue,'LineWidth',lw);
p32 = plot(t(1),x1_a(2),'.-','Color',Orange,'LineWidth',lw);
set(gca,'FontSize',fst)
leg3{1} = sprintf('$x_1(t)$');
leg3{2} = sprintf('$x_2(t)$');
legend(leg3,'Interpreter','latex','FontSize',fs,'Location','southwest')
title('States $\dot{x}(t) = Ax(t) + Bu(t-d)$','Interpreter','latex','FontSize',fs)
xlabel('time $(s)$','Interpreter','latex','FontSize',fs)
ymax = 1.1*max(max(max(abs(x1_a))),max(max(abs(x2_a))));
xlim([t(1),t(end)])
ylim([-ymax,ymax])
grid on


if save_figs
    figureDirectory = '../Figs/Sim_MainProblem';
    filename = 'Sim'; 

    if isfolder(figureDirectory) == 1 % If the file figureDirectory exists, delete it with all the files insisde
        rmdir(figureDirectory,'s');
        pause(0.1);
    end
    mkdir(figureDirectory); %Othercase, create the file figureDirectory
    pause(0.5);
    k = 1;
end



for i = 1:ceil(length(t)/Nfigs):length(t)
    set(p1,'XData',t(1:i),'YData',u(1:i))
    set(p2,'YData',w(:,i))
    set(p31,'XData',t(1:i),'YData',x1_a(1:i))
    set(p32,'XData',t(1:i),'YData',x2_a(1:i))

    if save_figs
        print(gcf,'-dpng','-r80',[figureDirectory,'/',filename,num2str(k),'.png']);
        k = k+1;
    end
    pause(0.01)
end




