%%% Plot parameters
save_figs = true;

lw = 2;
ms = 10;
fs  = 20;
fst = fs*0.8;
x0screen=30;y0screen=40;WidthScreen=1400;HeightScreen=300;
Blue = [0 0.4470 0.7410];
Orange = [0.8500 0.3250 0.0980];
Yellow = [0.9290 0.6940 0.1250];
Violet = [0.4940 0.1840 0.5560];
Green = [0.4660 0.6740 0.1880];

if save_figs
    Nfigs = 20;
else
    Nfigs = 500;
end