# Moving Mesh in 1D

## Getting started
We consider the following transport equation

```math
\begin{split}
 \dfrac{\partial w}{\partial t}(x,t) &= \dfrac{\partial w}{\partial x}(x,t), \\
 w(D,t) &= u(t), \\
 w(0,t) &= y(t)
 \end{split}
```

with $x \in [0,D]$, $t\geq 0$, and $w(x,y) \in \mathbb{R}$ being the spatial variable, the time variable and the state variable. The input is $u(t)$ and the output is $y(t)$. The output $y(t)$ is the input delayed in $D$ seconds, $i.e.$, $y(t) = u(t-D)$.

## Discretization scheme

Working on this ...

<!---
your comment goes here
and here

## Open-Loop Response

The following figure show the open-loop response. One can see two different behaviours due to the variation of $\sigma$. 

![This is an alt text.](/Figs/OL_Response.png "Open-Loop response.")

## Closed-Loop Responses using a PI controller

The objective is to design a single Proportional-Integral (PI) controller that stabilizes both cases $\sigma = \lbrace 1,2 \rbrace$. When dealing with this type of systems, one
can design a single PI controller using conventional tuning rules that stabilizes the worst case. However, the closed-loop stability and the performance is not always guaranteed for the second case. 

In the following figure, we show the closed-loop response when applying a PI controller designed using a conventional tuning rule (Amigo rules) considering the worst scenario $\sigma = 1$. One
can see that when $\sigma = 2$, the response is slow and it has an overshoot.

![This is an alt text.](/Figs/CL_Response1.png "Closed-Loop response conventional PI.")

Using the proposed approach, both models are considered for the design and a single PI controller can be tune in such a way that the closed-loop stability is guaranteed and a degree of performance
can be assigned considering a cost function.

![This is an alt text.](/Figs/CL_Response2.png "Closed-Loop response proposed PI.")

## References
The details have been published in the following two references:

[Toledo-Zucco, Sbarbaro, Gomes da Silva Jr, 2024](https://doi.org/10.48550/arXiv.2305.18605)

[Toledo-Zucco, Sbarbaro, 2018](https://doi.org/10.1016/j.ifacol.2018.11.157)
-->