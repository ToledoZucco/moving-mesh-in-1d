function Phi = Projection(zeta_d,zeta,N)

%     zeta = linspace(zeta_d(1),zeta_d(N),5000);
%     zeta_d = zeta;
    Phi = zeros(N,length(zeta));
    i = 1;
    for j = 1:length(zeta)
        if  (zeta_d(i) <= zeta(j) && zeta(j) <= zeta_d(i+1))
            Phi(i,j) = -zeta(j)/zeta_d(i+1) +1;
        end
    end
    for i = 2:N-1
        for j = 1:length(zeta)
            if  (zeta_d(i-1)<zeta(j) && zeta(j) <= zeta_d(i))
                Phi(i,j) = (zeta(j)-zeta_d(i-1))/(zeta_d(i)-zeta_d(i-1));
            end
            if  (zeta_d(i) < zeta(j) && zeta(j) <= zeta_d(i+1))
                Phi(i,j) = (-zeta(j)+zeta_d(i+1))/(zeta_d(i+1)-zeta_d(i)) ;
            end
        end
    end
    i = N;
    for j = 1:length(zeta)
        if  (zeta_d(i-1) < zeta(j) && zeta(j) <= zeta_d(i))
            Phi(i,j) = (zeta(j)-zeta_d(i-1))/(zeta_d(i)-zeta_d(i-1)) ;
        end
    end
end