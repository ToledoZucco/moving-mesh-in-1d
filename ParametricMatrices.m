close all
clear all
clc

%This code shows how to build the matrices E(t), A(t), B and C of the
%discretized one-dimensional transport equation using the Partitioned
%Finite Element (PFEM) and moving meshes.

%The finite-dimensional model is a Time-varying system of the following
%form:
% E(t)dxdt(t) = A(t)x(t) + Bu(t)
%        y(t) = C x(t)
% in which x(t) is the discretized varibale of x(z,t) from the Partial
% Differential Equation (PDE):
% dxdt(z,t) + c dedz(z,t) = 0, e = Hx

%For simplicity, in this code we consider c = 1 and H constant in space.
%This code has to be completed for the more general case.

%Developper     : Jesus-Pablo Toledo-Zucco
%Date           : 25/01/2024


%spatial boundaries
a = 0;
b = 1;
%Hamiltonian density
H = 1;
%velocity direction
c = 1;  %Velocity direction to the left side
% c = -1;  %Velocity direction to the right side
% Number of elements
N = 5;


%% Parametric Mass matrices E
ii = [1 2];
jj = [1 2];
ss = [-1 -1]/3;

ii = [ii,[1 2]];
jj = [jj,[2 1]];
ss = [ss,[-1 -1]/6];

Ei{1} = sparse(ii, jj, ss,N,N);
for i = 2:N-1

    ii = [i-1 i+1];
    jj = [i-1 i+1];
    ss = [1 -1]/3;

    ii = [ii,[i-1 i i i+1]];
    jj = [jj,[i i-1 i+1 i]];
    ss = [ss,[1 1 -1 -1]/6];

    Ei{i} = sparse(ii,jj,ss,N,N);
end
ii = [N-1 N];
jj = [N-1 N];
ss = [1 1]/3;
ii = [ii,[N-1 N]];
jj = [jj,[N N-1]];
ss = [ss,[1 1]/6];
Ei{N} = sparse(ii, jj,ss,N,N);
% The matrix E = Ei{1}*z(1) + Ei{2}*z(2) + ... + Ei{N}*z(N)
% For the case of a fixed equidistant mesh we have the following matrix E
h = (b-a)/(N-1);
z = a:h:b;

E = zeros(N,N);
for i = 1:N
    E = E + Ei{i}*z(i);
end


%% State matrix A.
%The state matrice is a little bit more complicated to compute since it is
%composed of three terms:
% A = A1 + A2 + A3
% A1 = - c\Phi_a\Phi_a^\top
% A2 = - c H \int_a^b \frac{\partial \Phi}{\partial z} \Phi ^\top dz
% A3 = - \int_a^b \Phi \frac{\partial \Phi}{\partial t}^\top dz

% A1 term
Phi_a = sparse(1,1,1,N,1);
A1 = -c*H*(Phi_a*Phi_a');

%A2 term
ii = [1     N       1:N-1               1+1:N];
jj = [1     N       1+1:N               1:N-1];
ss = [-1/2  1/2     -1/2*ones(1,N-1)    1/2*ones(1,N-1)];
A2 = -c*H*sparse(ii,jj,ss,N,N);

%A3
i = 1;
ii = [i i i+1 i+1];
jj = [i i+1 i i+1];
ss = [1/3 -1/3 1/6 -1/6];
A3i{1} = sparse(ii, jj, ss,N,N);
for i = 2:N-1

    ii = [i-1 i-1 i i i i+1 i+1];
    jj = [i-1 i i-1 i i+1 i i+1];
    ss = [1/6 -1/6 1/3 0 -1/3 1/6 -1/6];

    A3i{i} = sparse(ii,jj,ss,N,N);
end
i = N;
ii = [i-1 i-1   i   i ];
jj = [i-1 i     i-1 i ];
ss = [1/6 -1/6  1/3 -1/3];
A3i{i} = sparse(ii,jj,ss,N,N);
%A3 = A3i{1}*dz1dt + A3i{2}*dz2dt +....

% For the case of a constant mesh the time derivative of the nodes position
% is zero. Then, A3 becomes:
dzdt = zeros(N,1);

A3 = zeros(N,N);
for i = 1:N
    A3 = A3 + A3i{i}*dzdt(i);
end

A = A1 + A2 + A3;

%% Input matrix B
Phi_b = sparse(N,1,1,N,1);
B = c*Phi_b;

%% Output matrix C
C = Phi_a'*H;


